//
//  TimeLineViewController.m
//  TimelineTest
//
//  Created by xieyongjie on 2022/1/20.
//

#import "TimeLineViewController.h"
#import "Masonry.h"
#import "DNDownloadTimeLine.h"
#import "TimeLineView.h"
@interface TimeLineViewController ()
@property (nonatomic, strong) UIImageView *testImg;
@property (nonatomic, strong) DNDownloadTimeLine *timeLine;
@end

@implementation TimeLineViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
}

- (void)setupUI{
    [self.view addSubview:self.testImg];
    [self.view addSubview:self.timeLine];
    self.view.backgroundColor = [UIColor whiteColor];
    [self.testImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.view.mas_centerX);
        make.centerY.mas_equalTo(self.view.mas_centerY);
    }];
    
    [self.timeLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_left);
        make.right.mas_equalTo(self.view.mas_right);
        make.top.mas_equalTo(self.view.mas_top).offset(100);
        make.height.mas_equalTo(80);
    }];
}


- (UIImageView *)testImg{
    if (!_testImg) {
        _testImg = [[UIImageView alloc] init];
        _testImg.image = [UIImage imageNamed:@"testImg"];
    }
    return _testImg;
}

- (DNDownloadTimeLine *)timeLine{
    if (!_timeLine) {
        _timeLine = [[DNDownloadTimeLine alloc] init];
    }
    return _timeLine;
}
@end
