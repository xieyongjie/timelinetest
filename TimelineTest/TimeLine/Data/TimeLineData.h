//
//  TimeLineData.h
//  TimelineTest
//
//  Created by xieyongjie on 2022/1/24.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
typedef NS_ENUM(NSInteger, DeviceRecordType) {
    DeviceRecord_Normal = 1,        // 正常录像
    DeviceRecord_Alert,             // 告警录像
};

typedef NS_ENUM(NSInteger, DeviceVideoType) {
    VideoTypeCloud = 1,             // 云录像
    VideoTypeAlarm,
    VideoTypeSD,                    // SD卡录像
    VideoTypeTimeLineInfo,          // 时间轴
};

@interface TimeLineData : NSObject
//录像创建时间
@property (nonatomic, assign) int64_t create_time;
// 本段录像开始时间戳（毫秒）
@property (nonatomic, assign) int64_t start_time;
// 录像时长（毫秒）
@property (nonatomic, assign) int64_t time_len;
// 设备录像类型
@property (nonatomic, assign) DeviceRecordType record_type;

/** 云录像播放模型数组 */
@property (nonatomic, strong) NSMutableArray *cloudRecordModelArray;
/** 告警录像简略模型数组 */
@property (nonatomic, strong) NSMutableArray *alarmRecordModelArray;
/** sd录像简略模型数组 */
@property (nonatomic, strong) NSMutableArray *sdRecordModelArray;
- (NSMutableArray *)requestDataArrayWithCurrentTime:(int64_t)currentTime;
- (NSMutableArray *)requestCloudArrayWithCurrentTime:(int64_t)currentTime;
@end


@interface CloudRecordModel : TimeLineData

@end

@interface SDRecordModel : TimeLineData
// 录像时长（毫秒）
@property (nonatomic, assign) int64_t length;
@end

@interface AlarmRecordModel : TimeLineData

@end

@interface GetDevMsgTimeLineInfoModel : TimeLineData
// 录像时长（毫秒）
@property (nonatomic, assign) int64_t real_time_len;
@end


NS_ASSUME_NONNULL_END
