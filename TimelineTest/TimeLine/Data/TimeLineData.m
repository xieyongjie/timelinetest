//
//  TimeLineData.m
//  TimelineTest
//
//  Created by xieyongjie on 2022/1/24.
//

#import "TimeLineData.h"

@implementation CloudRecordModel

@end

@implementation SDRecordModel

@end

@implementation GetDevMsgTimeLineInfoModel

@end

@implementation AlarmRecordModel

@end

@implementation TimeLineData

- (instancetype)initWithType:(DeviceVideoType)type{
    TimeLineData *data;
    switch (type) {
        case VideoTypeCloud:
            data = [[CloudRecordModel alloc] init];
            break;
        case VideoTypeAlarm:
            data = [[AlarmRecordModel alloc] init];
            break;
        case VideoTypeSD:
            data = [[SDRecordModel alloc] init];
            break;
        case VideoTypeTimeLineInfo:
            data = [[GetDevMsgTimeLineInfoModel alloc] init];
            break;
            
        default:
            break;
    }
    
    return data;
}


- (NSMutableArray *)requestDataArrayWithCurrentTime:(int64_t)currentTime{
    int64_t beginTime = currentTime - 10 * 60000;
    NSMutableArray <GetDevMsgTimeLineInfoModel *> *alarmModelArray = [[NSMutableArray alloc] init];
    for (int i = 0; i < 8; i++) {
        GetDevMsgTimeLineInfoModel *recordModel = [[GetDevMsgTimeLineInfoModel alloc] init];
        recordModel.create_time = beginTime + i * 2 * 60000;
        recordModel.time_len = 1 * 60000;
        recordModel.real_time_len = 1 * 60000;
        [alarmModelArray addObject:recordModel];
    }
    return alarmModelArray;
}

- (NSMutableArray *)requestCloudArrayWithCurrentTime:(int64_t)currentTime{
    int64_t beginTime = currentTime - 10 * 60000;
    NSMutableArray <CloudRecordModel *> *cloudRecordModel = [[NSMutableArray alloc] init];
    for (int i = 0; i < 8; i++) {
        CloudRecordModel *recordModel = [[CloudRecordModel alloc] init];
        recordModel.create_time = beginTime + i * 2 * 60000;
        recordModel.time_len = 1 * 60000;
        [cloudRecordModel addObject:recordModel];
    }
    return cloudRecordModel;
}


@end

