//
//  UIView+Line.m
//  TimelineTest
//
//  Created by xieyongjie on 2022/1/21.
//

#import "UIView+Line.h"

@implementation UIView (Line)


-  (CAShapeLayer *)drawDoubleLine:(CGFloat)height distance:(CGFloat)distance start:(CGPoint)startPoint end:(CGPoint)endPoint{
    CAShapeLayer *layer = [CAShapeLayer layer];
    UIBezierPath *linePath = [UIBezierPath bezierPath];
    [linePath moveToPoint:startPoint];
    [linePath addLineToPoint:endPoint];
    CGPoint parStart = CGPointMake(startPoint.x, startPoint.y + distance);
    CGPoint parEnd = CGPointMake(endPoint.x, endPoint.y + distance);
    [linePath moveToPoint:parStart];
    [linePath addLineToPoint:parEnd];
    layer.lineWidth = height;
    layer.strokeColor = [UIColor blueColor].CGColor;
    layer.path = [linePath CGPath];
    return layer;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
