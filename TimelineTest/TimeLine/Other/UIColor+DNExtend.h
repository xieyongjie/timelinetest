//
//  UIColor+DNExtend.h
//  DanalePluginUI
//
//  Created by Jn on 2019/1/28.
//  Copyright © 2019年 DaiMaZhenYa. All rights reserved.
//

#import <UIKit/UIKit.h>

#define RGBA_COLOR(R, G, B, A) [UIColor colorWithRed:((R) / 255.0f) green:((G) / 255.0f) blue:((B) / 255.0f) alpha:A]
#define RGB_COLOR(R, G, B) [UIColor colorWithRed:((R) / 255.0f) green:((G) / 255.0f) blue:((B) / 255.0f) alpha:1.0f]

@interface UIColor (DNExtend)

+ (UIColor *)dn_ColorWithHexString:(NSString *)color;

//从十六进制字符串获取颜色，
//color:支持@“#123456”、 @“0X123456”、 @“123456”三种格式
+ (UIColor *)dn_ColorWithHexString:(NSString *)color alpha:(CGFloat)alpha;

+ (instancetype)dn_ColorWithHexString:(NSString *)color alpha:(CGFloat)alpha darkModeHex:(NSString *)darkColor darkAlpha:(CGFloat)darkAlpha;

@end
