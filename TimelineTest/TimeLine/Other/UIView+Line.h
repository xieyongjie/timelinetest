//
//  UIView+Line.h
//  TimelineTest
//
//  Created by xieyongjie on 2022/1/21.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIView (Line)

/**
 画一条平行线
 @param
 */
-  (CAShapeLayer *)drawDoubleLine:(CGFloat)height distance:(CGFloat)distance start:(CGPoint)startPoint end:(CGPoint)endPoint;
@end

NS_ASSUME_NONNULL_END
