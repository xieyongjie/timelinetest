//
//  TimeLineView.m
//  TimelineTest
//
//  Created by xieyongjie on 2022/1/20.
//

#import "TimeLineView.h"
#import "DNTimelineRuler.h"
#import "DownloadTimeRuler.h"
#import "TimeLineData.h"
#import "UIView+Line.h"
#import "Masonry.h"
#define UIColorFromRGBA(rgbValue,aValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:aValue]
#define DownloadStartTime ((self.contentView.contentOffset.x + self.leftView.frame.origin.x+14.75 - [UIScreen mainScreen].bounds.size.width/2)/self.ruler.widthOfSquare) * self.ruler.timeLengthOfSquare
#define DownloadEndTime ((self.contentView.contentOffset.x + self.rightView.frame.origin.x-0.25 - [UIScreen mainScreen].bounds.size.width/2)/self.ruler.widthOfSquare) * self.ruler.timeLengthOfSquare

@interface TimeLineView()<UIScrollViewDelegate>
/**刻度尺*/
@property (strong, nonatomic) DownloadTimeRuler *ruler;
/**底部滚动视图*/
@property (strong, nonatomic) UIScrollView *contentView;
/**选择框视图*/
@property (nonatomic , strong) UIView * selectBgView;
/**左边筛选视图*/
@property (nonatomic , strong) UIView * leftView;
/**右边筛选视图*/
@property (nonatomic , strong) UIView * rightView;
/**数据类*/
@property (nonatomic , strong) TimeLineData * data;
/** 下载范围 */
@property (nonatomic, strong) CAShapeLayer * selectedLayer;
//左边视图手势
@property (nonatomic, strong) UIPanGestureRecognizer *leftPanGestureRecognizer;
//右边视图手势
@property (nonatomic, strong) UIPanGestureRecognizer *rightPanGestureRecognizer;
@end

@implementation TimeLineView

- (instancetype)init{
    return  [self initWithFrame:CGRectZero];
}

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [self.leftView  addGestureRecognizer:self.leftPanGestureRecognizer];
        [self.rightView addGestureRecognizer:self.rightPanGestureRecognizer];
        NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc]init];
        style.alignment = NSTextAlignmentCenter;
        _timeTextStyleDictionary = @{NSFontAttributeName:[UIFont systemFontOfSize:11.0f],
                                     NSForegroundColorAttributeName:AHMUIC_TEXT_SECONDARY,
                                     NSParagraphStyleAttributeName:style};
        self.data = [[TimeLineData alloc] init];
        self.selectedLayer = [self.selectBgView drawDoubleLine:2 distance:28 start:CGPointMake(self.leftView.frame.origin.x+15, self.leftView.frame.origin.y+1) end:CGPointMake(self.rightView.frame.origin.x, self.rightView.frame.origin.y+1)];
        [self.selectBgView.layer addSublayer:self.selectedLayer];
        [self.ruler setDatas:[self.data requestDataArrayWithCurrentTime:[[NSDate date] timeIntervalSince1970]*1000]];
        [self.ruler setCloudData:[[NSMutableArray alloc] init]];
        [self.ruler setAlarmData:[self.data requestDataArrayWithCurrentTime:[[NSDate date] timeIntervalSince1970]*1000]];
        self.ruler.isMixtureData = NO;
        self.ruler.drawAlarmColor = YES;
        self.ruler.currentTime = [[NSDate date] timeIntervalSince1970];
        self.ruler.currentTimeInSecond = [[NSDate date] timeIntervalSince1970];
        [self setupUI];
        [self.ruler reloadView];
        self.selectBgView.userInteractionEnabled = NO;
        
        
       // [self drawRect:CGRectZero];
        
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [_ruler reloadViewWithFrame:self.contentView.frame zoomScale:_ruler.zoomScale];
}

- (UIView*)hitTest:(CGPoint)point withEvent:(UIEvent *)event{

    UIView *hitView = [super hitTest:point withEvent:event];

    if(hitView == self.selectBgView){

        return nil;

    }

    return hitView;

}

- (void)setupUI{
    [self addSubview:self.contentView];
    [self.contentView addSubview:self.ruler];
    //[self addSubview:self.ruler];
    [self addSubview:self.selectBgView];
    [self addSubview:self.leftView];
    [self addSubview:self.rightView];

    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.bottom.right.mas_equalTo(self);
    }];
    
//    [self.ruler mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.top.bottom.right.mas_equalTo(self);
//    }];
    
    
    [self.selectBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self);
        make.left.right.mas_equalTo(self);
        make.bottom.mas_equalTo(self.mas_bottom).offset(-20);
    }];
    
    self.leftView.frame = CGRectMake(self.frame.size.width/2, 10, 15, 30);
    self.rightView.frame = CGRectMake(self.frame.size.width/2+ 50, 10, 15, 30);
//    [self.leftView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(self.mas_left);
//        make.top.mas_equalTo(self.mas_top).offset(10);
//        make.width.mas_equalTo(15);
//        make.height.mas_equalTo(30);
//    }];
    
//    [self.rightView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.mas_equalTo(self.mas_right);
//        make.top.mas_equalTo(self.mas_top).offset(10);
//        make.width.mas_equalTo(15);
//        make.height.mas_equalTo(30);
//    }];
    
}

- (void)drawRect:(CGRect)rect{
    [super drawRect:rect];
    CGContextRef context = UIGraphicsGetCurrentContext();
    self.selectedLayer.path = [self.selectBgView drawDoubleLine:2 distance:28 start:CGPointMake(self.leftView.frame.origin.x+15, self.leftView.frame.origin.y+1) end:CGPointMake(self.rightView.frame.origin.x, self.rightView.frame.origin.y+1)].path;
    //填充区域
    CGRect middleRect = CGRectMake([UIScreen mainScreen].bounds.size.width *1/2, 0, 1, self.frame.size.height);
    [MIDDLE_LINE_COLOR setFill];
    CGContextFillRect(context, middleRect);
//    if (context) {
//        [[UIColor blackColor] set];
//        NSString  *scaleString = [NSString stringWithFormat:@"%f",(self.rightView.center.x - self.leftView.center.x -20)];
//        CGPoint point = CGPointMake(self.leftView.frame.origin.x+15, self.leftView.frame.origin.y+1);
//        [scaleString drawAtPoint:point withAttributes:_timeTextStyleDictionary];
//    }
}

/**
 左右时刻选择视图手势
 用来根据当前手势滑动改变选择框位置的记计算选择的时间戳
 */
- (void)dragViewMoved:(UIPanGestureRecognizer *)panGestureRecognizer {
    CGPoint leftTranslation;
    CGPoint rightTranslation;
    CGFloat leftViewX;
    CGFloat rightViewX;
    CGFloat tenMinLength;
    CGFloat screenRightEdge;
    if (panGestureRecognizer == self.leftPanGestureRecognizer) {
        leftTranslation  = [panGestureRecognizer translationInView:self.leftView];
    }
    if (panGestureRecognizer == self.rightPanGestureRecognizer) {
        rightTranslation = [panGestureRecognizer translationInView:self.rightView];
    }
    leftViewX = self.leftView.frame.origin.x + leftTranslation.x;
    rightViewX = self.rightView.frame.origin.x + rightTranslation.x;
    tenMinLength = self.ruler.widthOfSquare * (600/self.ruler.timeLengthOfSquare);
    screenRightEdge = [UIScreen mainScreen].bounds.size.width-self.rightView.bounds.size.width;
    
    if ((leftViewX <= (rightViewX - self.leftView.bounds.size.width)) && ((rightViewX - self.leftView.bounds.size.width) -leftViewX <= tenMinLength)) {
        if (leftViewX >= 0 && self.contentView.contentOffset.x + leftViewX +15 > [UIScreen mainScreen].bounds.size.width/2) {//左边视图不超出屏幕左边界
            self.leftView.center = CGPointMake(self.leftView.center.x + leftTranslation.x, self.leftView.center.y);
            [self drawRect:CGRectMake(self.leftView.center.x, self.leftView.center.y, self.frame.size.width, self.frame.size.height)];
        }
        if (rightViewX <= screenRightEdge && self.ruler.endTime/self.ruler.timeLengthOfSquare*self.ruler.widthOfSquare  > rightViewX + self.contentView.contentOffset.x  - [UIScreen mainScreen].bounds.size.width/2) {//右边视图不超过屏幕右边界
            self.rightView.center = CGPointMake(self.rightView.center.x + rightTranslation.x, self.rightView.center.y);
            [self drawRect:CGRectMake(self.leftView.center.x, self.leftView.center.y, self.frame.size.width, self.frame.size.height)];
        }
    }else if (((rightViewX - self.leftView.bounds.size.width) -leftViewX > tenMinLength) || ((rightViewX - self.leftView.bounds.size.width) -leftViewX <= 0) ){
        if (self.contentView.contentOffset.x + leftViewX + 15 -[UIScreen mainScreen].bounds.size.width/2 > 0 && self.ruler.endTime/self.ruler.timeLengthOfSquare*self.ruler.widthOfSquare > self.rightView.frame.origin.x + leftTranslation.x + self.contentView.contentOffset.x - [UIScreen mainScreen].bounds.size.width/2 && self.contentView.contentOffset.x + self.leftView.frame.origin.x + rightTranslation.x + 15 -[UIScreen mainScreen].bounds.size.width/2 > 0 && self.ruler.endTime/self.ruler.timeLengthOfSquare*self.ruler.widthOfSquare > rightViewX + self.contentView.contentOffset.x - [UIScreen mainScreen].bounds.size.width/2) {
            if (leftViewX >= 0 &&  self.rightView.frame.origin.x + leftTranslation.x <= screenRightEdge) {
                self.leftView.center = CGPointMake(self.leftView.center.x + leftTranslation.x, self.leftView.center.y);
                self.rightView.center = CGPointMake(self.rightView.center.x + leftTranslation.x, self.rightView.center.y);
                [self drawRect:CGRectMake(self.leftView.center.x, self.leftView.center.y, self.frame.size.width, self.frame.size.height)];
                
            }
            if (self.leftView.frame.origin.x + rightTranslation.x >= 0  && rightViewX <= screenRightEdge) {
                self.leftView.center = CGPointMake(self.leftView.center.x + rightTranslation.x, self.leftView.center.y);
                self.rightView.center = CGPointMake(self.rightView.center.x + rightTranslation.x, self.rightView.center.y);
                [self drawRect:CGRectMake(self.leftView.center.x, self.leftView.center.y, self.frame.size.width, self.frame.size.height)];
            }
        }
    }
    int64_t start = DownloadStartTime;
    int64_t end = DownloadEndTime;
    [self downloadStartTime:start endTime:end];
    [panGestureRecognizer setTranslation:CGPointZero inView:self];
}


//更新选择的开始&结束时间戳
- (void)downloadStartTime:(int64_t)start endTime:(int64_t)end{
    self.ruler.currentTimeInSecond = (int32_t)start;
    start = start * 1000 + self.ruler.startTime + [[self.ruler zeroOfDate:[[NSDate date] timeIntervalSince1970] ]timeIntervalSince1970]*1000;
    end = end * 1000 + self.ruler.startTime + [[self.ruler zeroOfDate:[[NSDate date] timeIntervalSince1970] ]timeIntervalSince1970]*1000;
    self.downloadBeginTime = start;
    self.downloadEndTime = end;
    if ([self.delegate respondsToSelector: @selector(showBeginTime: showEndTime:)]) {
        [self.delegate showBeginTime:start showEndTime:end];
    }
    NSLog(@"%lld - %lld" ,start ,end);
}


//MARK: scrollview Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (self.contentView.contentOffset.x + self.leftView.frame.origin.x+15 -[UIScreen mainScreen].bounds.size.width/2 < 0 ) {
        self.leftView.frame = CGRectMake([UIScreen mainScreen].bounds.size.width/2-self.contentView.contentOffset.x -15 , 10, 15, 30);
    }
    if (self.contentView.contentOffset.x + self.rightView.frame.origin.x -[UIScreen mainScreen].bounds.size.width/2 < 0) {
        self.rightView.frame = CGRectMake([UIScreen mainScreen].bounds.size.width/2-self.contentView.contentOffset.x , 10, 15, 30);
    }
    if (self.contentView.contentOffset.x + self.leftView.frame.origin.x+15 -[UIScreen mainScreen].bounds.size.width/2 > self.ruler.endTime/self.ruler.timeLengthOfSquare*self.ruler.widthOfSquare) {
        self.leftView.frame = CGRectMake(self.ruler.endTime/self.ruler.timeLengthOfSquare*self.ruler.widthOfSquare+[UIScreen mainScreen].bounds.size.width/2 -15 -self.contentView.contentOffset.x , 10, 15, 30);
    }
    if (self.contentView.contentOffset.x + self.rightView.frame.origin.x -[UIScreen mainScreen].bounds.size.width/2 > self.ruler.endTime/self.ruler.timeLengthOfSquare*self.ruler.widthOfSquare) {
        self.rightView.frame = CGRectMake(self.ruler.endTime/self.ruler.timeLengthOfSquare*self.ruler.widthOfSquare+[UIScreen mainScreen].bounds.size.width/2-self.contentView.contentOffset.x , 10, 15, 30);
    }
    
    [self drawRect:CGRectMake(self.leftView.center.x, self.leftView.center.y, self.frame.size.width, self.frame.size.height)];
    int64_t start = DownloadStartTime;
    int64_t end = DownloadEndTime;
    [self downloadStartTime:start endTime:end];
}
//MARK: getter
- (UIScrollView *)contentView{
    if (!_contentView) {
        _contentView = [[UIScrollView alloc] init];
        _contentView.bounces = NO;
        _contentView.delegate = self;
        _contentView.showsVerticalScrollIndicator = NO;
        _contentView.showsHorizontalScrollIndicator = NO;
    }
    return _contentView;
}

- (DownloadTimeRuler *)ruler{
    if (!_ruler) {
        _ruler = [[DownloadTimeRuler alloc] initWithFrame:CGRectZero];
    }
    
    return _ruler;
}

- (UIView *)selectBgView{
    if (!_selectBgView) {
        _selectBgView = [[UIView alloc] init];
        _selectBgView.backgroundColor = [UIColor clearColor];
    }
    return _selectBgView;
}

- (UIView *)leftView{
    if (!_leftView) {
        _leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 40)];
        UIImage *image = [UIImage imageNamed:@"leftSelected"];
        _leftView.layer.contents = (id) image.CGImage;
        _leftView.layer.backgroundColor = [UIColor clearColor].CGColor;
    }
    return  _leftView;
}

- (UIView *)rightView{
    if (!_rightView) {
        _rightView = [[UIView alloc] initWithFrame:CGRectMake(100, 0, 30, 40)];
        UIImage *image = [UIImage imageNamed:@"rightSelected"];
        _rightView.layer.contents = (id) image.CGImage;
        _rightView.layer.backgroundColor = [UIColor clearColor].CGColor;
    }
    return _rightView;
}

- (UIPanGestureRecognizer *)leftPanGestureRecognizer {
    if (!_leftPanGestureRecognizer) {
        _leftPanGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(dragViewMoved:)];
    }
    return _leftPanGestureRecognizer;
}

- (UIPanGestureRecognizer *)rightPanGestureRecognizer{
    if (!_rightPanGestureRecognizer) {
        _rightPanGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(dragViewMoved:)];
    }
    return _rightPanGestureRecognizer;
}

@end
