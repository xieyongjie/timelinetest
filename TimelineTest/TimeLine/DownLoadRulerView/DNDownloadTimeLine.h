//
//  DNDownloadTimeLine.h
//  TimelineTest
//
//  Created by xieyongjie on 2022/1/21.
//

#import <UIKit/UIKit.h>
#import "TimeLineView.h"
NS_ASSUME_NONNULL_BEGIN

@interface DNDownloadTimeLine : UIView
@property (nonatomic, strong) TimeLineView *timeLineView;
@end

NS_ASSUME_NONNULL_END
