//
//  TimeLineView.h
//  TimelineTest
//
//  Created by xieyongjie on 2022/1/20.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol TimeLineViewDelegate <NSObject>

- (void) showBeginTime:(int64_t)beginTime showEndTime:(int64_t)endTime;

@end

@interface TimeLineView : UIView
@property (strong,nonatomic) NSDictionary   *timeTextStyleDictionary; // 文本风格
@property (weak, nonatomic) id <TimeLineViewDelegate> delegate;
//下载开始时间
@property (assign,nonatomic) int64_t         downloadBeginTime;
//下载结束时间
@property (assign,nonatomic) int64_t         downloadEndTime;
@end

NS_ASSUME_NONNULL_END
