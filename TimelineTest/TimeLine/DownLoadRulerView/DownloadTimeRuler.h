//
//  DownloadTimeRuler.h
//  TimelineTest
//
//  Created by xieyongjie on 2022/1/24.
//

#import <UIKit/UIKit.h>
#define TOTAL_TIME_LENGTH           86400                                                          //总时长=20x60
#define DEFAULT_WIDTH_OF_SQUARE     [UIScreen mainScreen].bounds.size.width/120                    //默认缩放比例为1时每个格数的宽度
#define DEFAULT_SQUARE_PER_SCALE    6                                                              //默认每个刻度下含有的格数
#define DEFAULT_SCALE               1440*DEFAULT_SQUARE_PER_SCALE                                  //默认的总格数
#define DEFAULT_ALERT_COLOR         [UIColor dn_ColorWithHexString:@"#FF984F" alpha:0.3]
#define DEFAULT_NORMAL_COLOR        [UIColor dn_ColorWithHexString:@"#007DFF" alpha:0.1]
#define MIDDLE_LINE_COLOR           [UIColor dn_ColorWithHexString:@"#056fdc" alpha:1]
#define DEFAULT_CLEAR_COLOR         [UIColor clearColor]
#define UIColorFromRGBA(rgbValue,aValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:aValue]
#define AHMUIC_TEXT_SECONDARY               [UIColor dn_ColorWithHexString:@"#000000" alpha:0.6]
#define AHMUIC_TEXT_SECONDARY_NOALPHA       [UIColor dn_ColorWithHexString:@"#1A1A1A"]
#define AHMUIC_TEXT_SECONDARY_INVERSE       [UIColor dn_ColorWithHexString:@"#FFFFFF" alpha:0.6]
#define AHMUIC_TEXT_THIRD                   [UIColor dn_ColorWithHexString:@"#AAAAAA"]
#define kMaxZoomScale               3 //最大缩放比例
NS_ASSUME_NONNULL_BEGIN

@interface DownloadTimeRuler : UIView
// 当前播放时刻/刻度尺中心时刻
@property (assign,nonatomic) int64_t        currentTime;
// 刻度尺起始时间ms（默认选择当天0点)
@property (assign,nonatomic) int64_t        startTime;
// 刻度尺结束时间ms (默认选择当天24点)
@property (assign,nonatomic) int64_t        endTime;
// 全天录像开始时刻ms （混合时间轴）
@property (assign,nonatomic) int64_t        cloudBrginTime;
//每格时长 单位s
@property (assign,nonatomic) NSTimeInterval timeLengthOfSquare;
// 刻度起始时间毫秒
@property (assign,nonatomic) int32_t        currentTimeInSecond;
//云服务开通时间
@property (assign,nonatomic) int64_t        cloudServerStartTime;
//是否是混合数据
@property (assign,nonatomic) BOOL           isMixtureData;
//每格宽度,像素
@property (assign,nonatomic) CGFloat        widthOfSquare;
// 起始位置
@property (assign,nonatomic) CGFloat        startOriginX;
//刻度尺宽度
@property (assign,nonatomic) CGFloat        scaleWidth;
//刻度尺高度
@property (assign,nonatomic) CGFloat        scaleHeight;
//是否区分绘制正常数据的颜色（overlay不受影响）
@property (assign,nonatomic) BOOL           drawAlarmColor;
// 文本风格
@property (strong,nonatomic) NSDictionary   *timeTextStyleDictionary;
//刻度的宽度
@property (assign,nonatomic) CGFloat        scaleStringWidth;
//总格数
@property (assign,nonatomic) int            totalSquare;
//缩放比例
@property (assign,nonatomic) CGFloat        zoomScale;
//绘制区域
@property (assign,nonatomic) CGRect         rect;
//水平最大滚动的距离
@property (assign,nonatomic) int64_t        maxScrollDistance;
@property (strong,nonatomic) NSMutableArray *datas;//数据
@property (strong,nonatomic) NSMutableArray *alarmData;//数据
@property (strong,nonatomic) NSMutableArray *cloudData;//数据
@property (strong,nonatomic) NSMutableArray *overlayData;//数据
- (void)reloadView ;
- (void)reloadViewWithFrame:(CGRect)rect zoomScale:(int) zoomScale;
- (NSDate *)zeroOfDate:(int64_t)time;
@end

NS_ASSUME_NONNULL_END
