//
//  DNDownloadTimeLine.m
//  TimelineTest
//
//  Created by xieyongjie on 2022/1/21.
//

#import "DNDownloadTimeLine.h"
#import "Masonry.h"


@interface DNDownloadTimeLine()<TimeLineViewDelegate>
@property (nonatomic, strong) NSString *selectedTime;
@property (nonatomic, strong) UILabel *timeLb;
@end

@implementation DNDownloadTimeLine

- (instancetype)init{
    return  [self initWithFrame:CGRectZero];
}

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
    }
    return self;
}

- (void)setupUI{
    [self addSubview:self.timeLineView];
    [self addSubview:self.timeLb];
    
    [self.timeLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left);
        make.right.mas_equalTo(self.mas_right);
        make.top.mas_equalTo(self.mas_top);
        make.height.mas_equalTo(60);
    }];
    
    [self.timeLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.timeLineView.mas_bottom).offset(10);
        make.centerX.mas_equalTo(self.mas_centerX);
        make.width.mas_equalTo(300);
        make.height.mas_equalTo(30);
    }];
}

- (void) showBeginTime:(int64_t)beginTime showEndTime:(int64_t)endTime{
    
    NSLog(@"%@ ------ %@", [self timeOfDate:beginTime], [self timeOfDate:endTime]);
    self.selectedTime = [NSString stringWithFormat:@"%@ - %@",[self timeOfDate:beginTime], [self timeOfDate:endTime]];
    self.timeLb.text = self.selectedTime;
}

- (TimeLineView *)timeLineView{
    if (!_timeLineView) {
        _timeLineView = [[TimeLineView alloc] init];
        _timeLineView.delegate = self;
    }
    return _timeLineView;
}

-(UILabel *)timeLb{
    if (!_timeLb) {
        _timeLb = [[UILabel alloc] init];
        _timeLb.text = self.selectedTime;
        _timeLb.textAlignment = NSTextAlignmentCenter;
        _timeLb.font = [UIFont systemFontOfSize:16];
    }
    return _timeLb;
}

- (NSString *)timeOfDate:(int64_t)time
{
    
    NSDate * date = [NSDate dateWithTimeIntervalSince1970:time/1000];
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    format.dateFormat = @"HH:mm:ss";
    NSString *timeStr = [format stringFromDate:date];
    return timeStr;
}
@end
