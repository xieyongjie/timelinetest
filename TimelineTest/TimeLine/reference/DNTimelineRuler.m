//
//  DNTimelineRuler.m
//  TimelineTest
//
//  Created by xieyongjie on 2022/1/21.
//

#import "DNTimelineRuler.h"

@implementation DNTimelineRuler

- (instancetype)init{
    return [self initWithFrame:CGRectZero];
}

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor clearColor];
        NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc]init];
        style.alignment = NSTextAlignmentCenter;
        _timeTextStyleDictionary = @{NSFontAttributeName:[UIFont systemFontOfSize:11.0f],
                                     NSForegroundColorAttributeName:AHMUIC_TEXT_SECONDARY,
                                     NSParagraphStyleAttributeName:style};
        
        _scaleStringWidth = DEFAULT_WIDTH_OF_SQUARE * 8; //默认刻度文本宽度
        _drawAlarmColor = YES; // 默认画颜色
        _zoomScale = 1; // 默认缩放倍率1
        
        UITapGestureRecognizer *doubleTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action: @selector(handleDoubleTapOnView:)];
        [doubleTapRecognizer setNumberOfTapsRequired:2];
        [self addGestureRecognizer: doubleTapRecognizer];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [self reloadViewWithFrame:frame zoomScale:1];
        });
    }
    return self;
}

- (void)handleDoubleTapOnView:(UITapGestureRecognizer *)recognizer {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        self.zoomScale += 1;
        if (self.zoomScale > kMaxZoomScale) {
            self.zoomScale = 1;
        }
        //[self reloadViewWithFrame:self.rect zoomScale:self.zoomScale];
        [self scrollToTimeStamp:self.currentTimeInSecond animated:NO];
    });
}

- (void)reloadView {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
         [self reloadViewWithFrame:self.rect zoomScale:self.zoomScale];
    });
}

// MARK: 滚动到时间戳
- (void)scrollToTimeStamp:(int32_t)timeInSeconds animated:(BOOL)animated {
    dispatch_async(dispatch_get_main_queue(), ^{
        self.currentTimeInSecond = timeInSeconds;
        if (self.superview && [self.superview isKindOfClass:[UIScrollView class]]) {
            int32_t legalTime = MAX(0, MIN(timeInSeconds, TOTAL_TIME_LENGTH)); // boarder
            double targetOffsetX = ((double)legalTime / (TOTAL_TIME_LENGTH)) * (self.totalSquare * self.widthOfSquare);
            [((UIScrollView *)self.superview) setContentOffset:CGPointMake(targetOffsetX, 0) animated:animated];
        }
    });
}

- (void)reloadViewWithFrame:(CGRect)rect zoomScale:(int) zoomScale{
    if (CGRectEqualToRect(rect, CGRectZero)) return;
    self.rect = rect;
    //设置起始位置为中心位置
    _startOriginX = rect.size.width/2;
    //缩放比例
    _zoomScale     = zoomScale;
    //对应缩放比例下每格的宽度
    _widthOfSquare = DEFAULT_WIDTH_OF_SQUARE * _zoomScale;
    //总的格数
    _totalSquare   = DEFAULT_SCALE;
    //每格的时长
    _timeLengthOfSquare = TOTAL_TIME_LENGTH * 1.0f/_totalSquare;
    //总长度 = rect.size.width  + 总的格数*对应缩放比例下每格的宽度
    _scaleWidth  = rect.size.width + _totalSquare * _widthOfSquare ;
    _scaleHeight = rect.size.height;
    //设置父视图ScrollView的contentSize
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.superview && [self.superview isKindOfClass:[UIScrollView class]]) {
            ((UIScrollView *)self.superview).contentSize = CGSizeMake(self.scaleWidth, self.scaleHeight);
        }
    });
//    if (self.isMixtureData) {
//        if (_cloudData) {
//            id obj = _cloudData.lastObject;
//            NSTimeInterval time = 0 ;
//            if (obj && [obj isKindOfClass:[CloudRecordModel class]]) {
//                CloudRecordModel * model = obj;
//                time  = (model.start_time + model.time_len) * 1.0f/1000;//s
//            }else if (obj && [obj isKindOfClass:[SDRecordModel class]]){
//                SDRecordModel * model = obj;
//                time  = model.start_time + model.length;//s
//            }else if (obj && [obj isKindOfClass:[GetDevMsgTimeLineInfoModel class]]) {
//                GetDevMsgTimeLineInfoModel *model = obj;
////                time = model.create_time / 1000 + 50;
//                time = model.create_time / 1000 + model.real_time_len;
//            }
//            _maxScrollDistance = (time - _startTime / 1000 ) * 1.0f / _timeLengthOfSquare * _widthOfSquare;
//        }
//    }else {
//        if (_datas) {
//            id obj = _datas.lastObject;
//            NSTimeInterval time = 0 ;
//            if (obj && [obj isKindOfClass:[CloudRecordModel class]]) {
//                CloudRecordModel * model = obj;
//                time  = (model.start_time + model.time_len) * 1.0f/1000;//s
//            }else if (obj && [obj isKindOfClass:[SDRecordModel class]]){
//                SDRecordModel * model = obj;
//                time  = model.start_time + model.length;//s
//            }else if (obj && [obj isKindOfClass:[GetDevMsgTimeLineInfoModel class]]) {
//                GetDevMsgTimeLineInfoModel *model = obj;
////                time = model.create_time / 1000 + 50;
//                time = model.create_time / 1000 + model.real_time_len;
//            }
//            _maxScrollDistance = (time - _startTime / 1000 ) * 1.0f / _timeLengthOfSquare * _widthOfSquare;
//        }else{
//            _maxScrollDistance = 0;
//        }
//    }
    dispatch_async(dispatch_get_main_queue(), ^{
        self.frame = CGRectMake(0, 0, self.scaleWidth, self.scaleHeight);
        [self scrollToTimeStamp:self.currentTimeInSecond animated:NO]; // 尺寸发生变化的时候，保持住当前的位置
        [self setNeedsDisplay];
    });
}

- (void)drawRect:(CGRect)rect{
    [super drawRect:rect];
    CGContextRef context = UIGraphicsGetCurrentContext();
    //绘制刻度尺底色
    //[self drawRect:rect inContext:context];
    //绘制刻度尺
    NSString *scaleString = nil;
    CGFloat  originX      = 0;
    CGFloat  startOriginX  = _scaleStringWidth/2;
    CGRect  fillRect = CGRectZero;
    [UIColorFromRGBA(0xffffff,0.5) setFill];
    for (int i = 0 ; i <= _totalSquare; i++) {
        //第i个刻度所在的位置
        originX = _startOriginX + i * _widthOfSquare;
        //移动到需要绘制刻度线的起点
        if (i % DEFAULT_SQUARE_PER_SCALE == 0) {
            fillRect = CGRectMake(originX-1, _scaleHeight - _scaleHeight, 1, _scaleHeight);
            //绘制刻度
            scaleString = [self timeString: i * _timeLengthOfSquare];
            [scaleString drawInRect:CGRectMake(originX - startOriginX, _scaleHeight - 40, _scaleStringWidth, 20) withAttributes:_timeTextStyleDictionary];
        }else{
            fillRect = CGRectMake(originX-1, _scaleHeight - 13, 0.5, 13);
        }
        //填充区域
        CGContextFillRect(context, fillRect);
    }
}

//- (void)drawRect:(CGRect)rect inContext:(CGContextRef) context{
//    if (self.isMixtureData) {
//
//        id obj = _cloudData.firstObject;
//        id objAlarm = _alarmData.firstObject;
//
//        if ([obj isKindOfClass:[CloudRecordModel class]]) {
//            for (CloudRecordModel * model in _cloudData) {
//                //设置填充颜色
//                if (model.record_type == DeviceRecord_Normal || !self.drawAlarmColor) {
//                    [DEFAULT_NORMAL_COLOR setFill];
//                }else{
//                    [DEFAULT_ALERT_COLOR setFill];
//                }
//                //填充区域
//                CGContextFillRect(context, [self calculateRectForDataItem:model]);
//            }
//        }
//        if ([objAlarm isKindOfClass:[GetDevMsgTimeLineInfoModel class]]) {
//            if (!self.drawAlarmColor) {
//                [DEFAULT_CLEAR_COLOR setFill];
//            }else{
//                [DEFAULT_ALERT_COLOR setFill];
//            }
//            for (GetDevMsgTimeLineInfoModel *model in _alarmData) {
//                CGContextFillRect(context, [self calculateRectForDataItem:model]);
//            }
//        }
//    }else {
//        if (_datas && _datas.count > 0) {
//            id obj = _datas.firstObject;
//            if ([obj isKindOfClass:[CloudRecordModel class]]) {
//                for (CloudRecordModel * model in _datas) {
//                    //设置填充颜色
//                    if (model.record_type == DeviceRecord_Normal || !self.drawAlarmColor) {
//                        [DEFAULT_NORMAL_COLOR setFill];
//                    }else{
//                        [DEFAULT_ALERT_COLOR setFill];
//                    }
//                    //填充区域
//                    CGContextFillRect(context, [self calculateRectForDataItem:model]);
//                }
//            }else if ([obj isKindOfClass:[SDRecordModel class]]){
//                for (SDRecordModel * model in _datas) {
//                    //设置填充颜色
//                    if (model.record_type == 1 || !self.drawAlarmColor) {
//                        [DEFAULT_NORMAL_COLOR setFill];
//                    }else{
//                        [DEFAULT_ALERT_COLOR setFill];
//                    }
//                    //填充区域
//                    CGContextFillRect(context, [self calculateRectForDataItem:model]);
//                }
//            }else if ([obj isKindOfClass:[GetDevMsgTimeLineInfoModel class]]) {
//                if (!self.drawAlarmColor) {
//                    [DEFAULT_CLEAR_COLOR setFill];
//                }else{
//                    [DEFAULT_ALERT_COLOR setFill];
//                }
//                for (GetDevMsgTimeLineInfoModel *model in _datas) {
//                    CGContextFillRect(context, [self calculateRectForDataItem:model]);
//                }
//            }
//        }else{
//            //如果没有数据,则清除底色
//            CGContextClearRect(context, rect);
//            CGContextSaveGState(context);
//        }
//    }
//    // overlay
//    if (_overlayData && _overlayData.count > 0) {
//        id obj = _overlayData.firstObject;
//        if ([obj isKindOfClass:[GetDevMsgTimeLineInfoModel class]]) {
//            [DEFAULT_ALERT_COLOR setFill];
//            for (GetDevMsgTimeLineInfoModel *model in _overlayData) {
//                CGContextFillRect(context, [self calculateRectForDataItem:model]);
//            }
//        }
//    }
//}

///// 从列表数据中计算需要画的框
//- (CGRect)calculateRectForDataItem:(id)item {
//    CGRect rect = CGRectZero;
//    if ([item isKindOfClass:[GetDevMsgTimeLineInfoModel class]]) {
//        GetDevMsgTimeLineInfoModel *model = (GetDevMsgTimeLineInfoModel *)item;
//        NSTimeInterval second = (model.create_time - _startTime) / 1000;
//        if (second >= 0) {
//            CGFloat rectLeftEdgeX = second / _timeLengthOfSquare * _widthOfSquare + _startOriginX;
//            CGFloat rectWidth = 50 * 1.0f / _timeLengthOfSquare * _widthOfSquare; // clips 因为接口返回的长度为0，所以写死一个宽度
//            rect = CGRectMake(rectLeftEdgeX, 0, rectWidth, _scaleHeight);
//        }
//    } else if ([item isKindOfClass:[CloudRecordModel class]]) {
//        CloudRecordModel *model = (CloudRecordModel *)item;
//        NSTimeInterval second =  (model.start_time - _startTime) / 1000; //单位,s
//        if (second >= 0) {
//            CGFloat rectLeftEdgeX  = second / _timeLengthOfSquare * _widthOfSquare + _startOriginX;
//            int64_t recordEndTime = (model.start_time + model.time_len) / 1000;
//            NSTimeInterval length = model.time_len;
//            if(recordEndTime > (_startTime + TOTAL_TIME_LENGTH)){
//                length =  (_startTime + TOTAL_TIME_LENGTH) - model.start_time;
//            }
//            CGFloat rectWidth = length * 1.0f / 1000 / _timeLengthOfSquare * _widthOfSquare;
//            rect = CGRectMake(rectLeftEdgeX, 0, rectWidth, _scaleHeight);
//        }
//    } else if ([item isKindOfClass:[SDRecordModel class]]) {
//        SDRecordModel *model = (SDRecordModel *)item;
//        NSTimeInterval second = model.start_time - _startTime/1000;
//        if (second >= 0) {
//            CGFloat rectLeftEdgeX = second / _timeLengthOfSquare * _widthOfSquare + _startOriginX;
//            CGFloat rectWidth = model.length * 1.0f / _timeLengthOfSquare * _widthOfSquare;
//            rect = CGRectMake(rectLeftEdgeX, 0, rectWidth, _scaleHeight);
//        }
//    }
//    return rect;
//}

/**
 *  @author qicun, 16-01-04 10:01:01
 *  根据传入的时间戳计算刻度 单位s
 *
 *  @param time 时间戳
 *  @return 返回相应的刻度
 */
- (NSString *)timeString:(int64_t)time{
    return [NSString stringWithFormat:@"%02d:%02d",(int16_t)(time / 3600),(int16_t)(time % 3600 / 60)];
}

@end
