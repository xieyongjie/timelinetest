//
//  DNTimelineRuler.h
//  TimelineTest
//
//  Created by xieyongjie on 2022/1/21.
//

#import <UIKit/UIKit.h>
#import "UIColor+DNExtend.h"
#define TOTAL_TIME_LENGTH           86400                       //总时长=24x60x60
#define DEFAULT_WIDTH_OF_SQUARE     10.0f                       //默认缩放比例为1时每个格数的宽度
#define DEFAULT_SQUARE_PER_SCALE    6                           //默认每个刻度下含有的格数
#define DEFAULT_SCALE               24*DEFAULT_SQUARE_PER_SCALE //默认的总格数

#define DEFAULT_ALERT_COLOR         [UIColor dn_ColorWithHexString:@"#FF984F" alpha:0.3]
#define DEFAULT_NORMAL_COLOR        [UIColor dn_ColorWithHexString:@"#007DFF" alpha:0.1]
#define DEFAULT_CLEAR_COLOR         [UIColor clearColor]

#define DEFAULT_LAND_NORMAL_COLOR   [UIColor dn_ColorWithHexString:@"#007DFF" alpha:0.1]
#define DEFAULT_LAND_ALERT_COLOR    [UIColor dn_ColorWithHexString:@"#FF984F" alpha:0.3]
#define AHMUIC_BACKGROUND_BLACK_ALPHA05     [UIColor dn_ColorWithHexString:@"#000000" alpha:0.05]
#define AHMUIC_BACKGROUND_BLACK_ALPHA5      [UIColor dn_ColorWithHexString:@"#000000" alpha:0.05]
#define AHMUIC_BACKGROUND_BLACK_ALPHA10     [UIColor dn_ColorWithHexString:@"#000000" alpha:0.1]
#define AHMUIC_BACKGROUND_BLACK_ALPHA20     [UIColor dn_ColorWithHexString:@"#000000" alpha:0.2]
#define AHMUIC_BACKGROUND_BLACK_ALPHA40     [UIColor dn_ColorWithHexString:@"#000000" alpha:0.4]
#define AHMUIC_BACKGROUND_BLACK_ALPHA50     [UIColor dn_ColorWithHexString:@"#000000" alpha:0.5]
#define AHMUIC_BACKGROUND_BLACK_ALPHA80     [UIColor dn_ColorWithHexString:@"#000000" alpha:0.8]
#define AHMUIC_BACKGROUND_BLACK_ALPHA90     [UIColor dn_ColorWithHexString:@"#000000" alpha:0.9]
#define AHMUIC_BACKGROUND_BLACK             [UIColor dn_ColorWithHexString:@"#000000"]
#define AHMUIC_BACKGROUND_GRAY              [UIColor dn_ColorWithHexString:@"#F1F3F5"]
#define AHMUIC_BACKGROUND_WHITE             [UIColor dn_ColorWithHexString:@"#FFFFFF"]
#define AHMUIC_LINE_GRAY                    [UIColor dn_ColorWithHexString:@"#000000" alpha:0.2]
#define AHMUIC_SEPARATOR_LINE_GRAY          [UIColor dn_ColorWithHexString:@"#000000" alpha:0.05]
#define AHMUIC_BUTTON_GRAY                  [UIColor dn_ColorWithHexString:@"#000000" alpha:0.05]

#define AHMUIC_BACKGROUND_WHITE             [UIColor dn_ColorWithHexString:@"#FFFFFF"]
#define AHMUIC_TEXT_BLACK                   [UIColor dn_ColorWithHexString:@"#000000"]
#define AHMUIC_BACKGROUND_LAYER             [UIColor dn_ColorWithHexString:@"#FFFFFF" alpha:0.3]
#define AHMUIC_TEXT_WHITE                   [UIColor dn_ColorWithHexString:@"#FFFFFF"]
#define AHMUIC_TEXT_PRIMARY                 [UIColor dn_ColorWithHexString:@"#000000" alpha:0.9]
#define AHMUIC_TEXT_PRIMARY_INVERSE         [UIColor dn_ColorWithHexString:@"#FFFFFF" alpha:0.9]
#define AHMUIC_TEXT_SECONDARY               [UIColor dn_ColorWithHexString:@"#000000" alpha:0.6]
#define AHMUIC_TEXT_SECONDARY_NOALPHA       [UIColor dn_ColorWithHexString:@"#1A1A1A"]
#define AHMUIC_TEXT_SECONDARY_INVERSE       [UIColor dn_ColorWithHexString:@"#FFFFFF" alpha:0.6]
#define AHMUIC_TEXT_THIRD                   [UIColor dn_ColorWithHexString:@"#AAAAAA"]
#define AHMUIC_TEXT_RECORD_TIMER            [UIColor dn_ColorWithHexString:@"#F7F7F7"]
#define AHMUIC_TEXT_ORANGE_FREE_CLOUD       [UIColor dn_ColorWithHexString:@"#FF6012"]
#define AHMUIC_TEXT_WHITE_ALPHA60           [UIColor dn_ColorWithHexString:@"#FFFFFF" alpha:0.6]

#define AHMUIC_HARMONY_BLUE                 [UIColor dn_ColorWithHexString:@"#0A59F7"]
#define AHMUIC_HARMONY_BLUE_ALPHA30         [UIColor dn_ColorWithHexString:@"#0A59F7" alpha:0.3]
#define AHMUIC_HARMONY_BLUE_ALPHA10         [UIColor dn_ColorWithHexString:@"#0A59F7" alpha:0.1]
#define AHMUIC_HARMONY_BLUE_ALPHA60_DISABLE [UIColor dn_ColorWithHexString:@"#0A59F7" alpha:0.6]
#define AHMUIC_EMUI_BLUE                    [UIColor dn_ColorWithHexString:@"#007DFF"]
#define AHMUIC_EMUI_BLUE_ALPHA10            [UIColor dn_ColorWithHexString:@"#007DFF" alpha:0.1]
#define UIColorFromRGBA(rgbValue,aValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:aValue]
#define kMaxZoomScale               3 //最大缩放比例
NS_ASSUME_NONNULL_BEGIN

@interface DNTimelineRuler : UIView
@property (assign,nonatomic) CGFloat        scaleWidth;//刻度尺宽度
@property (assign,nonatomic) CGFloat        scaleHeight;//刻度尺高度
@property (assign,nonatomic) CGFloat        zoomScale;//缩放比例
@property (assign,nonatomic) int            totalSquare;//总格数
@property (assign,nonatomic) NSTimeInterval timeLengthOfSquare;//每格时长 单位s
@property (assign,nonatomic) CGFloat        widthOfSquare;//每格宽度,像素
@property (strong,nonatomic) NSDictionary   *timeTextStyleDictionary; // 文本风格
@property (assign,nonatomic) CGFloat        startOriginX; // 起始位置
@property (assign,nonatomic) CGFloat        scaleStringWidth;//刻度的宽度

@property (strong,nonatomic) NSMutableArray *datas;//数据
@property (strong,nonatomic) NSMutableArray *alarmData;//数据
@property (strong,nonatomic) NSMutableArray *cloudData;//数据
@property (strong,nonatomic) NSMutableArray *overlayData;//数据

@property (assign,nonatomic) int32_t        currentTimeInSecond;// 刻度起始时间毫秒

@property (assign,nonatomic) int64_t        startTime;// 刻度起始时间毫秒
@property (assign,nonatomic) CGRect         rect;//绘制区域
@property (assign,nonatomic) int64_t        maxScrollDistance;//水平最大滚动的距离

@property (assign,nonatomic) int64_t        cloudServerStartTime;//云服务开通时间
@property (assign,nonatomic) BOOL           isMixtureData;//是否是混合数据
@property (assign,nonatomic) BOOL           drawAlarmColor;//是否区分绘制正常数据的颜色（overlay不受影响）
- (void)reloadView ;
- (void)reloadViewWithFrame:(CGRect)rect zoomScale:(int) zoomScale;
@end

NS_ASSUME_NONNULL_END
